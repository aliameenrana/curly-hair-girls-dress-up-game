﻿using UnityEngine;
using System.Collections;

public class sa : MonoBehaviour {

	void Start ()
    {
	
	}
	
	void Update ()
    {
        if (Input.GetMouseButtonDown(0))
        {
            StartCoroutine(Fader(this.gameObject));
        }
        if (Input.GetMouseButtonDown(1))
        {
            StartCoroutine(UnFader(this.gameObject));
        }
    }

    public IEnumerator Fader(GameObject gameObject)
    {
        while (gameObject.GetComponent<SpriteRenderer>().color.a > 0)
        {
            gameObject.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, Time.deltaTime * 3);
            yield return null;
        }
    }
    public IEnumerator UnFader(GameObject gameObject)
    {
        while (gameObject.GetComponent<SpriteRenderer>().color.a < 1)
        {
            gameObject.GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, Time.deltaTime * 3);
            yield return null;
        }
    }
}
