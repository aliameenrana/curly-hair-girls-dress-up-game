﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.UI;
using UnityEngine.EventSystems;
using DG.Tweening;
using System.IO;

public class UI_Control : MonoBehaviour {
    #region Declarations
    public GameObject Button_Play, Button_Store, Button_Settings, Button_Quit, Button_GetFreeCoins, NameField, Button_Enter, Panel_MainMenu, ModelSelection_BG, Panel_ModelSelection, Panel_Shower, SkinTone0, SkinTone1, SkinTone2, SkinTone3, Panel_MakeUp_Main, Panel_MakeUp_Sidebar, SelectDressScene;
    public GameObject SelectedModel, Table;
    public GameObject ShoweringScene, DressUpScene;
    public GameObject HairStyle, LipStick, LipGloss, EyeLens, HairBand, Glasses, EyeBrows, Earrings, EyeShadow, EyeLiner, EyeLashes, Blushes;
    public GameObject BodySkinTone0, BodySkinTone1, BodySkinTone2, BodySkinTone3;
    public Text NameFieldText;
    public string UserName;
    public bool Faded=false, HandInstantiated=false;
    public static UI_Control instance;
    public enum Stage {ShowerStage, MakeUpStage, DressUpStage};
    public Stage CurrentStage;
    int ScreenShotCount; bool ScreenShotTaken=false; string ScreenShotName, LatestPath;
    public List<GameObject> AppliedMakeUp;
    public bool MenuOpen = false; public Sprite MenuIcon, CrossIcon; public GameObject MenuButton, VolumeButton, CartButton, HomeButton, ScreenShotButton, RateButton;
    Vector3 CamPos, VolPos, HomePos, CartPos, RatePos;
    public GameObject ScreenShotFrame;
    public Canvas MainCanvas;
    public Text Deb;
    #endregion Declarations

    void Awake()
    {
        instance = this;
        Button_Play = GameObject.Find("btn_HomeScreen_Play");
        Button_Store = GameObject.Find("btn_HomeScreen_Store");
        Button_Settings = GameObject.Find("btn_HomeScreen_Settings");
        Button_Quit = GameObject.Find("btn_HomeScreen_Quit");
        Button_GetFreeCoins = GameObject.Find("btn_HomeScreen_GetFreeCoins");
        Button_Enter = GameObject.Find("btn_HomeScreen_Enter");
        Panel_MainMenu = GameObject.Find("Panel_MainMenu");
        NameField = GameObject.Find("NameField");
        Panel_ModelSelection = GameObject.Find("Panel_ModelSelection");
        Panel_Shower = GameObject.Find("Panel_Shower");
        DressUpScene = GameObject.Find("MakeUpScene");
        SelectDressScene = GameObject.Find("SelectDressScene");
    }
    void Start()
    {
        NameField.SetActive(false);
        Button_Enter.SetActive(false);
        DressUpScene.SetActive(false);
        SelectDressScene.SetActive(false);
        HidePanel(Panel_ModelSelection);
        VolPos = VolumeButton.transform.position;
        CamPos = ScreenShotButton.transform.position;
        RatePos = RateButton.transform.position;
        CartPos = CartButton.transform.position;
        HomePos = HomeButton.transform.position;
        VolumeButton.SetActive(false);
        CartButton.SetActive(false);
        HomeButton.SetActive(false);
        ScreenShotButton.SetActive(false);
        RateButton.SetActive(false);
        ScreenShotFrame.SetActive(false);
    }
    void Update()
    {
        
    }
    public void PlayGame()
    {
        StartCoroutine(TakeMenuAway());
        NameField.SetActive(true);
        NameField.transform.position = new Vector3(NameField.transform.position.x, NameField.transform.position.y + 20, NameField.transform.position.z);
        Button_Enter.SetActive(true);
        Button_Enter.transform.position = new Vector3(Button_Enter.transform.position.x, Button_Enter.transform.position.y - 10, Button_Enter.transform.position.z);
        iTween.MoveTo(NameField, new Vector3(0, 0, 0), 0.5f);
        iTween.MoveTo(Button_Enter, new Vector3(0, -1, 0), 0.75f);
    }
    public void EnterGame()
    {
        UserName = NameFieldText.text;
        bool fieldMoving = false;


        if (NameFieldText.text != "")
        {
            fieldMoving = true;
            iTween.MoveBy(NameField, new Vector3(0, 1090, 0), 0.75f);
            iTween.MoveBy(Button_Enter, new Vector3(0, -1090, 0), 0.75f);
            StartCoroutine(FadeOut(Panel_MainMenu));
            StartCoroutine(FadeIn(Panel_ModelSelection, Panel_MainMenu, ""));
        }
        if (fieldMoving == false)
        {
            EventSystem.current.SetSelectedGameObject(NameField.gameObject, null);
        }
    }
    public void ModelSelect()
    {
        SkinTone0.SetActive(false);
        SkinTone1.SetActive(false);
        SkinTone2.SetActive(false);
        SkinTone3.SetActive(false);
        
        string Name = EventSystem.current.currentSelectedGameObject.GetComponent<Transform>().name;
        StartCoroutine(FadeOut(Panel_ModelSelection));
        StartCoroutine(FadeIn(Panel_Shower, Panel_ModelSelection, Name));
        ShowerScene.instance.HoverHand(ShowerScene.instance.Shampoo);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1,1,1,1);
        ShowerScene.instance.Hand.transform.position = new Vector3(ShowerScene.instance.Shampoo.transform.position.x - 0.25f, ShowerScene.instance.Shampoo.transform.position.y - 0.25f, ShowerScene.instance.Shampoo.transform.position.z);
        
        CurrentStage = Stage.ShowerStage;
    } 
    public void HidePanel(GameObject gameObject)
    {
        gameObject.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        foreach (Transform item in gameObject.transform)
        {
            item.GetComponent<Image>().color = new Color(1, 1, 1, 0);
        }
    }
    public void LoadPrevScene()
    {
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        if (CurrentStage == Stage.ShowerStage)
        {
            if (MenuOpen == true)
            {
                OpenMenu();
            }
            foreach (Transform item in ShoweringScene.transform)
            {
                if (item.gameObject.tag == "Foam")
                {
                    item.gameObject.SetActive(false);
                }
                else if (item.gameObject.tag == "Drop")
                {
                    item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                }
            }
            StartCoroutine(FadeIn(Panel_ModelSelection, Panel_Shower, ""));
        }
        else if (CurrentStage == Stage.MakeUpStage)
        {
            Debug.Log(Panel_MakeUp_Sidebar);
            Panel_MakeUp_Sidebar.SetActive(false);
            Panel_MakeUp_Main.SetActive(false);
            foreach (Transform item in DressUpScene.transform)
            {
                if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0), 0.5f).OnComplete(() => item.gameObject.SetActive(false));
                }
            }
            foreach (Transform item in ShoweringScene.transform)
            {
                if (item.gameObject.tag != "Drop" && item.gameObject.tag != "WetHair" && item.gameObject.tag != "Model" && item.gameObject.tag != "Foam")
                {
                    item.gameObject.SetActive(true);
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                }
                else if (item.gameObject.tag == "Model")
                {
                    if (SelectedModel == SkinTone0 && item.gameObject.name == "Image_SkinTone0")
                    {
                        item.gameObject.SetActive(true);
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                    }
                    else if (SelectedModel == SkinTone1 && item.gameObject.name == "Image_SkinTone1")
                    {
                        item.gameObject.SetActive(true);
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                    }
                    else if (SelectedModel == SkinTone2 && item.gameObject.name == "Image_SkinTone2")
                    {
                        item.gameObject.SetActive(true);
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                    }
                    else if (SelectedModel == SkinTone3 && item.gameObject.name == "Image_SkinTone3")
                    {
                        item.gameObject.SetActive(true);
                        item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                    }
                }
            }
            ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            ShowerScene.instance.Hand.transform.position = ShowerScene.instance.Shampoo.transform.position;
            CurrentStage = Stage.ShowerStage;
        }
        else if (CurrentStage == Stage.DressUpStage)
        {
            Debug.Log("Going back...");
            foreach (Transform item in SelectDressScene.transform)
            {
                item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 0),0.5f);
            }
            DressUpScene.SetActive(true);
            Panel_MakeUp_Main.SetActive(true);
            Panel_MakeUp_Sidebar.SetActive(true);
            foreach (Transform item in DressUpScene.transform)
            {
                foreach (GameObject item2 in AppliedMakeUp)
                {
                    if (item2 == item.gameObject)
                    {
                        item.gameObject.SetActive(true);
                        //item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                        if (item.childCount > 1)
                        {
                            item.GetChild(0).gameObject.SetActive(true);
                            item.GetChild(1).gameObject.SetActive(true);
                            item.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                            item.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                        }
                    }
                }

                if (item.gameObject.name == "Img_SkinTone0" && SelectedModel == SkinTone0)
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "Img_SkinTone1" && SelectedModel == SkinTone1)
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "Img_SkinTone2" && SelectedModel == SkinTone2)
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "Img_SkinTone3" && SelectedModel == SkinTone3)
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "Image_Table")
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "Image_MakeUp")
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.name == "DressUpSceneBackground")
                {
                    item.gameObject.SetActive(true);
                }
                if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                }
            }
            CurrentStage = Stage.MakeUpStage;
        }
    }
    public void LoadNextScene()
    {
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        if (CurrentStage == Stage.ShowerStage)
        {
            DressUpScene.SetActive(true);
            foreach (Transform item in DressUpScene.transform)
            {
                if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
                }
            }
            StartCoroutine(FadeSprite(ShoweringScene));
            StartCoroutine(UnFadeSprite(DressUpScene));
            StartCoroutine(FadeOut(Panel_Shower));
            StartCoroutine(FadeIn(Panel_MakeUp_Main, Panel_Shower, ""));
            StartCoroutine(FadeIn(HairStyle, Panel_Shower, ""));
            Panel_MakeUp_Sidebar = HairStyle;
            CurrentStage = Stage.MakeUpStage;
        }
        else if (CurrentStage == Stage.MakeUpStage)
        {
            SelectDressScene.SetActive(true);
            if (SelectedModel == SkinTone0)
            {
                BodySkinTone0.SetActive(true);
                BodySkinTone0.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                BodySkinTone1.SetActive(false);
                BodySkinTone2.SetActive(false);
                BodySkinTone3.SetActive(false);
            }
            else if (SelectedModel == SkinTone1)
            {
                BodySkinTone1.SetActive(true);
                BodySkinTone1.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                BodySkinTone0.SetActive(false);
                BodySkinTone2.SetActive(false);
                BodySkinTone3.SetActive(false);
            }
            else if (SelectedModel == SkinTone2)
            {
                BodySkinTone2.SetActive(true);
                BodySkinTone2.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                BodySkinTone1.SetActive(false);
                BodySkinTone3.SetActive(false);
                BodySkinTone0.SetActive(false);
            }
            else if (SelectedModel == SkinTone3)
            {
                BodySkinTone3.SetActive(true);
                BodySkinTone3.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                BodySkinTone0.SetActive(false);
                BodySkinTone1.SetActive(false);
                BodySkinTone2.SetActive(false);
            }

            AppliedMakeUp = new List<GameObject>();
            foreach (Transform item in DressUpScene.transform)
            {
                if ((item.gameObject.activeSelf == true) && (item.gameObject.tag != "BG") && (item.gameObject.tag != "Model") && (item.gameObject.name != "Image_Table"))
                {
                    AppliedMakeUp.Add(item.gameObject);
                }
            }

            foreach (GameObject item in AppliedMakeUp)
            {
                foreach (Transform item2 in SelectDressScene.transform)
                {
                    if (item2.gameObject.name == ("A_" + item.name))
                    {
                        item2.gameObject.SetActive(true);
                        item2.gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.5f);
                        if (item2.childCount > 1)
                        {
                            item2.GetChild(0).gameObject.SetActive(true);
                            item2.GetChild(1).gameObject.SetActive(true);
                            item2.GetChild(0).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.4f);
                            item2.GetChild(1).gameObject.GetComponent<SpriteRenderer>().DOColor(new Color(1, 1, 1, 1), 0.4f);
                        }
                    }
                }
            }
            StartCoroutine(FadeSprite(DressUpScene));
            foreach (Transform item in SelectDressScene.transform)
            {
                if (item.gameObject.name == "DressUpSceneBackground")
                {
                    item.gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
                }
            }
            CurrentStage = Stage.DressUpStage;
            Panel_MakeUp_Main.SetActive(false);
            Panel_MakeUp_Sidebar.SetActive(false);
        }
        else if (CurrentStage == Stage.DressUpStage)
        {
            Debug.Log("You are on the last stage...");
        }
    }
    public void Deactivate(GameObject gameObject)
    {
        gameObject.SetActive(false);
    }
    public void Shot()
    {
        if (CurrentStage == Stage.DressUpStage)
        {
            StartCoroutine(TakeScreenShot());
        }
    }
    public void Rec()
    {
        StopAllCoroutines();
        var bytesRead = System.IO.File.ReadAllBytes(ScreenShotName);
        Deb.text = bytesRead[0].ToString();
        Texture2D myTexture = new Texture2D(982, 1746);
        myTexture.LoadImage(bytesRead);
        ScreenShotFrame.GetComponent<Image>().sprite = Sprite.Create(myTexture, new Rect(0, 0, myTexture.width, myTexture.height), new Vector2(0.5f, 0.5f));
        ScreenShotFrame.SetActive(true);
    } 
    public void OpenMenu()
    {
        MenuOpen = !MenuOpen;
        if (MenuOpen == true)
        {
            MenuButton.GetComponent<Image>().sprite = CrossIcon;
            VolumeButton.SetActive(true);
            CartButton.SetActive(true);
            HomeButton.SetActive(true);
            ScreenShotButton.SetActive(true);
            RateButton.SetActive(true);
            
            VolumeButton.transform.position = MenuButton.transform.position;
            VolumeButton.transform.DOMove(VolPos, 0.1f, false);
            
            ScreenShotButton.transform.position = MenuButton.transform.position;
            ScreenShotButton.transform.DOMove(CamPos, 0.15f, false);
            RateButton.transform.position = MenuButton.transform.position;
            RateButton.transform.DOMove(RatePos, 0.2f, false);
            
            CartButton.transform.position = MenuButton.transform.position;
            CartButton.transform.DOMove(CartPos, 0.1f, false);
            
            HomeButton.transform.position = MenuButton.transform.position;
            HomeButton.transform.DOMove(HomePos, 0.15f, false);
        }
        else if (MenuOpen == false)
        {
            MenuButton.GetComponent<Image>().sprite = MenuIcon;
            VolumeButton.transform.DOMove(MenuButton.transform.position, 0.1f, false).OnComplete(() => VolumeButton.SetActive(false));
            ScreenShotButton.transform.DOMove(MenuButton.transform.position, 0.15f, false).OnComplete(() => ScreenShotButton.SetActive(false));
            RateButton.transform.DOMove(MenuButton.transform.position, 0.2f, false).OnComplete(() => RateButton.SetActive(false));
            CartButton.transform.DOMove(MenuButton.transform.position, 0.1f, false).OnComplete(() => CartButton.SetActive(false));
            HomeButton.transform.DOMove(MenuButton.transform.position, 0.15f, false).OnComplete(() => HomeButton.SetActive(false));
        } 
    }

    public IEnumerator TakeMenuAway()
    {
        float Duration = 2.5f;
        float StartTime = Time.time;

        while (Button_Play.transform.position.x > -1000)
        {
            while (Time.time - StartTime < Duration)
            {
                Button_Play.transform.localPosition = Vector3.Lerp(Button_Play.transform.localPosition, new Vector3(Button_Play.transform.localPosition.x - 1000, Button_Play.transform.localPosition.y, Button_Play.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 3f)));
                Button_Store.transform.localPosition = Vector3.Lerp(Button_Store.transform.localPosition, new Vector3(Button_Store.transform.localPosition.x - 1000, Button_Store.transform.localPosition.y, Button_Store.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 2)));
                Button_Settings.transform.localPosition = Vector3.Lerp(Button_Settings.transform.localPosition, new Vector3(Button_Settings.transform.localPosition.x - 1000, Button_Settings.transform.localPosition.y, Button_Settings.transform.localPosition.z), ((Time.time - StartTime) / (Duration)));
                Button_Quit.transform.localPosition = Vector3.Lerp(Button_Quit.transform.localPosition, new Vector3(Button_Quit.transform.localPosition.x - 1000, Button_Quit.transform.localPosition.y, Button_Quit.transform.localPosition.z), ((Time.time - StartTime) / (Duration * 0.25f)));
                Button_GetFreeCoins.transform.localPosition = Vector3.Lerp(Button_GetFreeCoins.transform.localPosition, new Vector3(Button_GetFreeCoins.transform.localPosition.x + 1000, Button_GetFreeCoins.transform.localPosition.y, Button_GetFreeCoins.transform.localPosition.z), ((Time.time - StartTime) / Duration));
                yield return null;
            }
            yield return null;
        }
    }
    public IEnumerator Translate(GameObject gameObject, Vector3 InitPos, Vector3 FinalPos, float Duration)
    {
        float StartTime = Time.time;

        while (Time.time - StartTime < Duration)
        {
            gameObject.transform.localPosition = Vector3.Lerp(gameObject.transform.localPosition, FinalPos, ((Time.time - StartTime) / (Duration * 3f)));
            yield return null;
        }
    }
    public IEnumerator FadeOut(GameObject gameObject)
    {
        while (gameObject.GetComponent<Image>().color.a >= 0)
        {
            gameObject.GetComponent<Image>().color -= new Color(0, 0, 0, Time.deltaTime * 2);
            foreach (Transform item in gameObject.transform)
            {
                if (item.gameObject.GetComponent<Image>() != null)
                {
                    item.gameObject.GetComponent<Image>().color -= new Color(0, 0, 0, Time.deltaTime * 3);
                }
            }
            yield return null;
        }
        gameObject.SetActive(false);
    }
    public IEnumerator FadeIn(GameObject NewMenu, GameObject OldMenu, string Name)
    {
        NewMenu.SetActive(true);
        Color RefColor = new Color(1, 1, 1, 1);
        if (NewMenu != Panel_ModelSelection)
        {
            foreach (Transform item in NewMenu.transform)
            {
                if (item.gameObject.GetComponent<SpriteRenderer>() != null)
                {
                    item.gameObject.GetComponent<SpriteRenderer>().color += new Color(0, 0, 0, (RefColor.a - item.gameObject.GetComponent<SpriteRenderer>().color.a) / 0.5f);
                }
            }
        }
        switch (Name)
        {
            case "ModelSelection_BlueDress":
                {
                    SelectedModel = SkinTone0;
                    SelectedModel.SetActive(true);
                    break;
                }
            case "ModelSelection_GreenDress":
                {
                    SelectedModel = SkinTone1;
                    SelectedModel.SetActive(true);
                    break;
                }
            case "ModelSelection_WhiteDress":
                {
                    SelectedModel = SkinTone2;
                    SelectedModel.SetActive(true);
                    break;
                }
            case "ModelSelection_BlackDress":
                {
                    SelectedModel = SkinTone3;
                    SelectedModel.SetActive(true);
                    break;
                }                
        }
        if (NewMenu.GetComponent<Image>() != null)
        {
            while (NewMenu.GetComponent<Image>().color.a < 1)
            {
                if (NewMenu == Panel_ModelSelection)
                {
                    NewMenu.GetComponent<Image>().color += new Color(0, 0, 0, Time.deltaTime * 2);
                }
                foreach (Transform item in NewMenu.transform)
                {
                    if (item.gameObject.GetComponent<Image>() != null)
                    {
                        item.gameObject.GetComponent<Image>().color += new Color(0, 0, 0, Time.deltaTime * 3);
                    }
                }
                yield return null;
            }
        }
        OldMenu.SetActive(false);
        yield return null;
    }
    public IEnumerator FadeSprite(GameObject Sprites)
    {
        SpriteRenderer[] Rends = Sprites.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer item in Rends)
        {
            if (item.gameObject.tag != "Model")
            {
                item.DOColor(new Color(1, 1, 1, 0), 0.5f);
            }
            else if (item.gameObject.tag == "Model" || item.gameObject.tag == "Hair" || item.gameObject.tag == "BG")
            {
                item.color = new Color(1,1,1,0);
            }
        }
        yield return new WaitForSeconds(0.6f);
        foreach (SpriteRenderer item in Rends)
        {
            item.gameObject.SetActive(false);
        }
    }
    public IEnumerator UnFadeSprite(GameObject Sprites)
    {
        Sprites.SetActive(true);
        SpriteRenderer[] Rends = Sprites.GetComponentsInChildren<SpriteRenderer>();
        foreach (SpriteRenderer item in Rends)
        {
            if (item.gameObject.tag != "Drop" && item.gameObject.tag != "WetHair")
            {
                if (item.gameObject.tag != "Model" && item.gameObject.tag != "Hair")
                {
                    item.DOColor(new Color(1, 1, 1, 1), 0.1f);
                }
                else if (item.gameObject.tag == "Model" || item.gameObject.tag == "Hair"|| item.gameObject.tag == "BG")
                {
                    if (item.gameObject.tag == "Hair")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                    if (SelectedModel == SkinTone0 && item.gameObject.name == "Img_SkinTone0")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone1 && item.gameObject.name == "Img_SkinTone1")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone2 && item.gameObject.name == "Img_SkinTone2")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                    else if (SelectedModel == SkinTone3 && item.gameObject.name == "Img_SkinTone3")
                    {
                        item.color = new Color(1, 1, 1, 1);
                    }
                }
            }
        }
        yield return null;
    }
    public IEnumerator TakeScreenShot()
    {
        //MainCanvas.enabled = false;
        if (ScreenShotFrame.activeSelf == true)
        {
            ScreenShotFrame.SetActive(false);
        }
        ScreenShotCount++;
        ScreenShotName = Application.persistentDataPath + "/Screenshot__" + ScreenShotCount + System.DateTime.Now.ToString("__yyyy-MM-dd") + ".png";
        Application.CaptureScreenshot(ScreenShotName);
        ScreenShotTaken = true;
        Deb.text = ScreenShotName;
        yield return new WaitUntil(() => File.ReadAllBytes(ScreenShotName) != null);
        Rec();
        //MainCanvas.enabled = true;
    }
}