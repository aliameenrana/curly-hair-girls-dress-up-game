﻿using UnityEngine;
using System.Collections;

public class ParticlesController : MonoBehaviour {
    public static ParticlesController instance;
    void Awake()
    {
        instance = this;
    }
    void Start ()
    {
        transform.GetComponent<ParticleSystem>().enableEmission = false;
	}	
	void Update ()
    {
        transform.position = new Vector3(ShowerController.instance.transform.position.x-0.25f, ShowerController.instance.transform.position.y+0.75f, ShowerController.instance.transform.position.z);
	}
}
