﻿using UnityEngine;
using System.Collections;

public class TowelController : MonoBehaviour {

    public Camera SpritesCam;
    public Vector3 OrigPosition;
    public GameObject Shadow, Hair;
    public Sprite TowelOpen;
    public Sprite TowelFolded;
    public bool HairIsWet = false;
    public bool OnHair = false;
    public static TowelController instance;

    void Awake()
    {
        instance = this;
    }
    void Start()
    {
        OrigPosition = transform.position;
    }
    void Update()
    {
        if (OnHair == true)
        {
            Debug.Log("Towel On Hair");
        }
    }
    void OnMouseDown()
    {

    }
    void OnMouseDrag()
    {
        transform.position = new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z + 10);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        transform.GetComponent<SpriteRenderer>().sprite = TowelOpen;
        transform.localScale = new Vector3(1,1,1);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        if (ShowerScene.instance.Hair.GetComponent<Collider2D>().bounds.Contains(ShowerScene.instance.Towel.GetComponent<Collider2D>().bounds.center))
        {
            ShowerScene.instance.WetHair.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, Time.deltaTime);
            GameObject[] Drops = GameObject.FindGameObjectsWithTag("Drop");
            foreach (GameObject item in Drops)
            {
                item.SetActive(true);
                item.GetComponent<SpriteRenderer>().color -= new Color(0, 0, 0, Time.deltaTime*0.25f);
            }
        }
    }
    void OnMouseUp()
    {
        iTween.MoveTo(transform.gameObject, OrigPosition, 0.5f);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        transform.GetComponent<SpriteRenderer>().sprite = TowelFolded;
        transform.localScale = new Vector3(2, 2, 2);
        ShowerScene.instance.Dragging = false;
        ShowerScene.instance.WetHair.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        GameObject[] Drops = GameObject.FindGameObjectsWithTag("Drop");
        foreach (GameObject item in Drops)
        {
            item.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
            item.SetActive(false);
        }
    }
    void OnTriggerStay2D(Collider2D other)
    {
        
    }
}
