﻿using UnityEngine;
using System.Collections;

public class ShampooController : MonoBehaviour {
    public static ShampooController instance;
    public Camera SpritesCam;
    public Vector3 OrigPosition;
    public GameObject Shadow;
    public bool ScrubbingAvailable = false;
    void Awake()
    {
        Shadow = GameObject.Find("Image_ShampooShadow");
        instance = this;
    }
    void Start ()
    {
        OrigPosition = transform.position;
    }	
	void Update ()
    {
	
	}
    void OnMouseDown()
    {
        
    }
    void OnMouseDrag()
    {
        transform.position = new Vector3(SpritesCam.ScreenToWorldPoint(Input.mousePosition).x, SpritesCam.ScreenToWorldPoint(Input.mousePosition).y, SpritesCam.ScreenToWorldPoint(Input.mousePosition).z+10);
        transform.rotation = Quaternion.Euler(0, 0, 30);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0);
    }
    void OnMouseUp()
    {
        iTween.MoveTo(transform.gameObject, OrigPosition, 0.5f);
        transform.rotation = Quaternion.Euler(0, 0, 0);
        Shadow.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        ShowerScene.instance.Dragging = false;
        ShowerScene.instance.Hand.transform.position = new Vector3(ShowerScene.instance.Hair.transform.position.x, ShowerScene.instance.Hair.transform.position.y +3f, ShowerScene.instance.Hair.transform.position.z);
        ShowerScene.instance.Hand.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        ScrubbingAvailable = true;
    }
}
