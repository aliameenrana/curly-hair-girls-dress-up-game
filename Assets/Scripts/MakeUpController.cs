﻿using UnityEngine;
using System.Collections;
using UnityEngine.EventSystems;
using System.Collections.Generic;

public class MakeUpController : MonoBehaviour {
    public static MakeUpController instance;
    public GameObject SelectedMakeup, HairStyle, LipStick, LipGloss, HairAccessory, EyeLens, EyeBrow, EyeShade, EyeLiner, EyeLash, EarRing, Blush, ClickedMakeUp;
    public GameObject[] Panels, HairStyles, LipSticks, LipGlosses, HairAccessories, EyeLenses, EyeBrows, EyeShades, EyeLiners, EyeLashes, EarRings, Blushes, Glasses;
    public GameObject[] Sprite_HairStyles, Sprite_LipSticks, Sprite_LipGlosses, Sprite_HairAccessories, Sprite_EyeLenses, Sprite_EyeBrows, Sprite_EyeShades, Sprite_EyeLiners, Sprite_EyeLashes, Sprite_EarRings, Sprite_Blushes, Sprite_Glasses;
    public Dictionary<string,GameObject[]> MakeUp;
    public Dictionary<string, GameObject[]> Sprite_MakeUp;
    public GameObject uiControllerObj;

    void Awake()
    {
        instance = this;
    }
	void Start ()
    {
        MakeUp = new Dictionary<string, GameObject[]>();
        Sprite_MakeUp = new Dictionary<string, GameObject[]>();
        #region
        Panels = new GameObject[11];
        Panels[0] = HairStyle;
        Panels[1] = LipStick;
        Panels[2] = LipGloss;
        Panels[3] = HairAccessory;
        Panels[4] = EyeLens;
        Panels[5] = EyeBrow;
        Panels[6] = EyeShade;
        Panels[7] = EyeLiner;
        Panels[8] = EyeLash;
        Panels[9] = EarRing;
        Panels[10] = Blush;
        #endregion
        #region
        MakeUp.Add("HairStyle", HairStyles);
        MakeUp.Add("LipStick", LipSticks);
        MakeUp.Add("LipGloss", LipGlosses);
        MakeUp.Add("EyeLens", EyeLenses);
        MakeUp.Add("HairAccessories", HairAccessories);
        MakeUp.Add("Glasses", Glasses);
        MakeUp.Add("EyeBrows", EyeBrows);
        MakeUp.Add("EarRing", EarRings);
        MakeUp.Add("EyeShadow", EyeShades);
        MakeUp.Add("EyeLiner", EyeLiners);
        MakeUp.Add("EyeLashes", EyeLashes);
        MakeUp.Add("Blushes", Blushes);
        #endregion
        #region
        Sprite_MakeUp.Add("HairStyle", Sprite_HairStyles);
        Sprite_MakeUp.Add("LipStick", Sprite_LipSticks);
        Sprite_MakeUp.Add("LipGloss", Sprite_LipGlosses);
        Sprite_MakeUp.Add("EyeLens", Sprite_EyeLenses);
        Sprite_MakeUp.Add("HairAccessories", Sprite_HairAccessories);
        Sprite_MakeUp.Add("Glasses", Sprite_Glasses);
        Sprite_MakeUp.Add("EyeBrows", Sprite_EyeBrows);
        Sprite_MakeUp.Add("EarRing", Sprite_EarRings);
        Sprite_MakeUp.Add("EyeShadow", Sprite_EyeShades);
        Sprite_MakeUp.Add("EyeLiner", Sprite_EyeLiners);
        Sprite_MakeUp.Add("EyeLashes", Sprite_EyeLashes);
        Sprite_MakeUp.Add("Blushes", Sprite_Blushes);
        #endregion
    }
    void Update ()
    {
	
	}
    public void OnClickMain()
    {
        ClickedMakeUp = EventSystem.current.currentSelectedGameObject;
        SelectedMakeup = ClickedMakeUp;
        if (SelectedMakeup.name == "HairStyle")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            HairStyle.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = HairStyle;
        }
        else if (SelectedMakeup.name == "LipStick")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            LipStick.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = LipStick;
        }
        else if (SelectedMakeup.name == "LipGloss")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            LipGloss.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = LipGloss;
        }
        else if (SelectedMakeup.name == "HairAccessories")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            HairAccessory.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = HairAccessory;
        }
        else if (SelectedMakeup.name == "EyeLenses")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EyeLens.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EyeLens;
        }
        else if (SelectedMakeup.name == "EyeBrows")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EyeBrow.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EyeBrow;
        }
        else if (SelectedMakeup.name == "EyeShade")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EyeShade.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EyeShade;
        }
        else if (SelectedMakeup.name == "EyeLiner")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EyeLiner.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EyeLiner;
        }
        else if (SelectedMakeup.name == "EyeLashes")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EyeLash.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EyeLash;
        }
        else if (SelectedMakeup.name == "EarRing")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            EarRing.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = EarRing;
        }
        else if (SelectedMakeup.name == "Blushes")
        {
            foreach (GameObject item in Panels)
            {
                item.SetActive(false);
            }
            Blush.SetActive(true);
            uiControllerObj.GetComponent<UI_Control>().Panel_MakeUp_Sidebar = Blush;
        }
    }
    public void OnClickMakeUp()
    {
        GameObject Clicked = EventSystem.current.currentSelectedGameObject;
        GameObject[] Temp, Temp2;
        MakeUp.TryGetValue(Clicked.tag, out Temp);
        int i = 0;
        foreach (GameObject item in Temp)
        {
            if (item.name == Clicked.name)
            {
                break;
            }
            i++;
        }
        Sprite_MakeUp.TryGetValue(Clicked.tag, out Temp2);
        if (Clicked.tag == "HairStyle")
        {
            foreach (GameObject item in Temp2)
            {
                item.SetActive(false);
            }
            Temp2[i + 1].SetActive(true);
            Temp2[i + 1].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
        else if (Clicked.tag == "LipStick" || Clicked.tag == "LipGloss" || Clicked.tag == "HairAccessories" || Clicked.tag == "Blushes")
        {
            foreach (GameObject item in Temp2)
            {
                item.SetActive(false);
            }
            Temp2[i].SetActive(true);
            if (Clicked.tag == "LipGloss")
            {
                Temp2[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 0.3f);
            }
            else if (Clicked.tag == "LipStick" || Clicked.tag == "HairAccessories" || Clicked.tag == "Blushes")
            {
                Temp2[i].GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            }
            if (Temp2[i].name == "HairBand")
            {
                Temp2[i].transform.position = new Vector3(-0.4f, 1.23f, 0);
            }
            if (Temp2[i].name == "HairBand (3)" || Temp2[i].name == "HairBand (4)" || Temp2[i].name == "HairBand (5)")
            {
                Temp2[i].transform.position = new Vector3(0f, 0.78f, 0);
            }
        }
        else if (Clicked.tag == "EyeLens" || Clicked.tag == "EyeBrows" || Clicked.tag == "EyeShadow" || Clicked.tag == "EyeLiner" || Clicked.tag == "EyeLashes" || Clicked.tag == "EarRing")
        {
            foreach (GameObject item in Temp2)
            {
                item.SetActive(false);
            }
            Temp2[i].SetActive(true);
            Temp2[i].transform.GetChild(0).gameObject.SetActive(true);
            Temp2[i].transform.GetChild(0).gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
            Temp2[i].transform.GetChild(1).gameObject.SetActive(true);
            Temp2[i].transform.GetChild(1).gameObject.GetComponent<SpriteRenderer>().color = new Color(1, 1, 1, 1);
        }
    }
}
